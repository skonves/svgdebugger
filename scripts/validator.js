var Validator = function(){

  var cpx=0,cpy=0,
      sx=0, sy=0;

  var validationDefs = {
    'M': {
      name: 'Move To',
      parameters: {
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'Z': {
      name: 'Close Path',
      parameters: {}
    },
    'L': {
      name: 'Line To',
      parameters: {
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'H': {
      name: 'Horizontal Line To',
      parameters: {
        'x': validateAsNumber
      }
    },
    'V': {
      name: 'Vertical Line To',
      parameters: {
        'y': validateAsNumber
      }
    },
    'C': {
      name: 'Cubic Bezier Curve',
      parameters: {
        'x1': validateAsNumber,
        'y1': validateAsNumber,
        'x2': validateAsNumber,
        'y2': validateAsNumber,
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'S': {
      name: 'Smooth Cubic Bezier Curve',
      parameters: {
        'x2': validateAsNumber,
        'y2': validateAsNumber,
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'Q': {
      name: 'Quadradic Curve',
      parameters: {
        'x1': validateAsNumber,
        'y1': validateAsNumber,
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'T': {
      name: 'Smooth Quadradic Curve',
      parameters: {
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    },
    'A': {
      name: 'Elliptical Arc',
      parameters: {
        'rx': validateAsNonNegative,
        'ry': validateAsNonNegative,
        'x-axis-rotation': validateAsNumber,
        'large-arc-flag': validateAsFlag,
        'sweep-flag': validateAsFlag,
        'x': validateAsNumber,
        'y': validateAsNumber
      }
    }
  };

  this.Test = function(pathData){
    var commands = ParsePath(pathData);
    var validatedCommands = Validate(commands);
    return validatedCommands;
  }

  this.ValidatePath = function(pathData) {
    return pathData ? Validate(ParsePath(pathData)) : [];
  }

  this.ParsePath = function(pathData) {
    var start = 0,
        commandPos = -1, // init with value less than start
        results = [];

    cpx = 0;
    cpy = 0;

    for(var i = start; i < pathData.length; i++) {
      var c = pathData.charAt(i);
      var endOfCommand = i === pathData.length - 1 || isLetter(pathData.charAt(i+1));

      if(isLetter(c)){
        commandPos = i;
      }

      if(endOfCommand && commandPos >= start){
        results.push(createCommand(pathData, commandPos, start, i - start + 1));
        start = i + 1;
      }
    }

    return results;
  }

  this.ParseParameters = function(paramString, offset){
    var start = 0,
        canYield = false;
        dotIsNewParam = false;
        results = [];

    for(var i = start; i < paramString.length; i++){
      var c = paramString.charAt(i);
      var cc = paramString.charAt(i+1);

      if(!dotIsNewParam && c==='.' && isDigit(cc)){
        dotIsNewParam = true;
      } else if(dotIsNewParam && c==='.') {
        dotIsNewParam = false;
      }

      var endOfParam =
        i === paramString.length - 1
        || (isWhitespace(c) && !isWhitespace(cc))
        || (isDigit(c) && cc === '-')
        || (dotIsNewParam && cc ==='.');

      canYield = canYield || !isWhitespace(c);

      if(canYield && endOfParam) {
        results.push(createParameter(paramString,start,i-start +1,offset));
        start = i+1;
        dotIsNewParam = false;
      }
    }

    return results;
  }

  this.Validate = function(commands){
    var results = [];

    commands.forEach(function(command){
      var c = command.command;
      var def = validationDefs[c.toUpperCase()];


        results.push(GetValid(command,def));
    });

    return results;
  }

  this.GetValid = function(command, def){
    var commandErrors = [];
    var paramGroupErrors = [];
    var paramGroups = [];
    var c = command.command;
    var paramNames = def ? Object.keys(def.parameters) : [];

    var groupSize = paramNames.length > 0 ? paramNames.length : command.parameters.length;
    var groupStart = 0;
    var groupLength = 0;
    var validParams = [];
    var p = 0;
    var startPoint;

    if(!def){
      var index = (command.start + command.text.indexOf(c));
      commandErrors.push(createError('Unknown command "' + c + '" at index ' + index, index, 1 ));
    }

    if(paramNames.length > 0 && (!command.parameters || command.parameters.length === 0)) {
      var index = (command.start + command.length);
      var message = 'Expected parameter "' + paramNames[0] + '" at index ' + index;
      commandErrors.push(createError(message, index, 1 ));
    }

    if(paramNames.length === 0 && command.parameters.length > 0) {
        var index = (command.start + command.length);
        var message = 'Parameters not allowed for command "'+c+'"';
        commandErrors.push(createError(message, index, command.parameters.length ));
    }

    for(var i = 0; i < command.parameters.length; i++) {
      var param = command.parameters[i];
      groupLength += param.length;

      // First param
      if(p === 0) {
        groupStart = param.start;
        startPoint = {x:cpx,y:cpy};
      }

      // Handle param
      var name = paramNames.length > 0 ? paramNames[p] : null;
      var point = createValidatedPoint(command, name, i);

      validParams.push(createValidatedParam(param, name, def, point));

      // Last param
      if(p === groupSize - 1) {
        var lines = createValidatedLines(command,validParams,startPoint);
        paramGroups.push(createValidatedParamGroup(validParams, command, groupStart, groupLength, paramGroupErrors, lines ));
        validParams = [];
        groupLength = 0;
      }

      // Incomplete group and last param
      if(p !== groupSize - 1 && i === command.parameters.length - 1) {
        var index = groupStart + groupLength;
        var message = 'Expected parameter "' + paramNames[p+1] + '" at index ' + index;

        var error = createError(message, index, 1);
        paramGroups.push(createValidatedParamGroup(validParams,command,groupStart,groupLength,[error]));
      }

      if(++p === groupSize){
        p = 0;
      }
    }

    var validCommand = createValidatedCommand(command, paramGroups, def, commandErrors);
    return validCommand;
  }

  function createCommand(pathData, commandPos, start, length) {
    var paramString = pathData.substring(commandPos + 1, start + length);
    return {
      command: pathData.substring(commandPos, commandPos + 1),
      paramString: paramString,
      parameters: ParseParameters(paramString,commandPos+1),
      text: pathData.substring(start, start + length),
      start: start,
      length: length
    };
  }

  function createParameter(paramString, start, length, offset) {
    var text = paramString.substring(start, start+length);
    return {
      value: getParameterValue(text),
      text: text,
      start: start + offset,
      length: length
    };
  }

  function createError(message, index, length) {
    return {
      message: message,
      index: index,
      length: length
    };
  }

  function createValidatedCommand(command, paramGroups, def, errors) {
    var isRel = isRelative(command.command);
    var name = def ? def.name : null;

    return {
      name: name,
      char: command.command,
      isRelative: isRel,
      paramGroups: paramGroups,
      errors: !errors || errors.length === 0 ? null : errors,
      text: command.text,
      start: command.start,
      length: command.length
    };
  }

  function createValidatedParamGroup(validatedParams, command, groupStart, groupLength, errors, lines) {
    errors = errors ? errors : [];
    var offset = groupStart - command.start;
    var text = command.text.substring(offset, groupLength+1);

    return {
      parameters: validatedParams,
      errors: !errors || errors.length === 0 ? null : errors,
      lines: !lines || lines.length === 0 ? null : lines,
      text: text,
      start: groupStart,
      length: groupLength
    };
  }

  function createValidatedParam(parameter, name, def, point) {
    var errors = [];
    var validator = def && def.parameters[name] ? def.parameters[name] : validateAsNumber;
    var errors = validator(parameter);
    return {
      name: name,
      value: parameter.value,
      point: point,
      errors: !errors || errors.length === 0 ? null : errors,
      text: parameter.text,
      start: parameter.start,
      length: parameter.length
    };
  }

  function createValidatedPoint(command, paramName, i){
    var char = command.command;
    var isRel = isRelative(char);
    var x,y;

    if (char === 'H' || char === 'h') {
      x = command.parameters[i].value + (isRel ? cpx : 0);
      y = cpy;
      cpx = x;
    } else if (char === 'V' || char === 'v') {
      x = cpx;
      y = command.parameters[i].value + (isRel ? cpy : 0);
      cpy = y;
    } else if (isFirstOfPair(paramName) && i !== command.parameters.length - 1) {
      x = command.parameters[i].value + (isRel ? cpx : 0);
      y = command.parameters[i+1].value + (isRel ? cpy : 0);
      if(paramName === 'x') {

      }
    } else if (isSecondOfPair(paramName)) {
      x = command.parameters[i-1].value + (isRel ? cpx : 0);
      y = command.parameters[i].value + (isRel ? cpy : 0);
      if(paramName === 'y') {
        cpx = x;
        cpy = y;
      }
    } else {
      return;
    }

    return {
      x: x,
      y: y
    };
  }

  function createValidatedLines(command, validParams, startPoint){
    var c = command.command;

    if(c==='C'||c==='c'){
      return [
        {x1:startPoint.x, y1:startPoint.y, x2:validParams[0].point.x, y2:validParams[0].point.y},
        {x1:validParams[4].point.x, y1:validParams[4].point.y, x2:validParams[2].point.x, y2:validParams[2].point.y}
      ];
    } else if(c==='S'||c==='s'){
      return [
        // TODO: add implicit line
        {x1:validParams[2].point.x, y1:validParams[2].point.y, x2:validParams[0].point.x, y2:validParams[0].point.y}
      ];
    } else if(c==='Q'||c==='q'){
      return [
        {x1:startPoint.x, y1:startPoint.y, x2:validParams[0].point.x, y2:validParams[0].point.y},
        {x1:validParams[2].point.x, y1:validParams[2].point.y, x2:validParams[0].point.x, y2:validParams[0].point.y}
      ];
    } else if(c==='T'||c==='t'){
      return [
        // TODO: add implicit lines
      ];
    }
  }

  function getParameterValue(str) {
    var value = (0-str.replace(/,/,'').trim())*(-1);

    return value === -0 ? 0 : value;
  }

  function validateAsNumber(parameter) {
    var errors = [];

    if(isNaN(parameter.value)) {
      errors.push(createError('Expected numerical value at index ' + parameter.start + '. ("' + parameter.text +'" is not a valid numerical value.)', parameter.start, parameter.length))
    }

    return errors;
  }

  function validateAsNonNegative(parameter) {
    var errors = validateAsNumber(parameter);

    if(errors.length === 0 && parameter.value < 0) {
      errors.push(createError('Expected non-negative value at index ' + parameter.start + '. ("' + parameter.text +'" is not a valid non-negative value.)', parameter.start, parameter.length))
    }

    return errors;
  }

  function validateAsFlag(parameter) {
    var errors = validateAsNumber(parameter);

    if(errors.length === 0 && parameter.value !== 0 && parameter.value !== 1) {
      errors.push(createError('Expected flag (0 or 1) at index ' + parameter.start + '. ("' + parameter.text +'" is not a valid flag.)', parameter.start, parameter.length))
    }

    return errors  ;
  }

  function isLetter(char){
    return char.length === 1 && char.match(/[a-df-z]/i);
  }

  function isDigit(char){
    return char.length === 1 && char.match(/[0-9]/i);
  }

  function isWhitespace(char){
    return char.length === 1 && (char===',' || char.trim() === '');
  }

  function isRelative(char){
    return char.length === 1 && char.toUpperCase() !== char;
  }

  function isFirstOfPair(paramName){
    return paramName === 'x' || paramName === 'x1' || paramName === 'x2'
  }

  function isSecondOfPair(paramName){
    return paramName === 'y' || paramName === 'y1' || paramName === 'y2'
  }

  return this;
}();
